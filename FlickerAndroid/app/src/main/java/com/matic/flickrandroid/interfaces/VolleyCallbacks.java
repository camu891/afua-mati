package com.matic.flickrandroid.interfaces;

/**
 * Created by matic on 1/9/17.
 */

public interface VolleyCallbacks {
    void VolleyResponse(String data);
    void responseUserInfo(String data);
}
