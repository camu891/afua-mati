package com.matic.flickrandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.interfaces.PhotoClickListener;
import com.matic.flickrandroid.model.PhotoGallery;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Afua on 15/9/2017.
 */

public class ListGalleryAdapter extends RecyclerView.Adapter<ListGalleryAdapter.GalleryViewHolder> {
    private static final String TAG = ListGalleryAdapter.class.getSimpleName();
    private List<PhotoGallery> galleryItemList;
    private Context mContext;
    private PhotoClickListener photoClickListener;

    public ListGalleryAdapter(List<PhotoGallery> galleryItemList, Context mContext, PhotoClickListener photoClickListener) {
        this.galleryItemList = galleryItemList;
        this.mContext = mContext;
        this.photoClickListener = photoClickListener;
    }


    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //Encargado de crear los nuevos objetos ViewHolder necesarios para los elementos de la colección.
        //nos limitaremos a inflar una vista a partir del layout correspondiente a los elementos de la lista
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, null);
        return new GalleryViewHolder(view, photoClickListener);
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        // Encargado de actualizar los datos de un ViewHolder ya existente.
        PhotoGallery gallery = galleryItemList.get(position);
        holder.id = gallery.getId();
        holder.url = gallery.getPhotoUrl();
        holder.title = gallery.getTitle();
        holder.tvTitle.setText(gallery.getTitle());
        setImage(gallery.getPhotoUrl(), holder);
    }

    public void clear() { //
        if (galleryItemList != null) {
            galleryItemList.clear();
            notifyDataSetChanged();
        }
    }

    public void addAll(List<PhotoGallery> list) { //agrega todas las imagenes
        Log.d(TAG, "addAll: list:" + list);
        if (galleryItemList != null) {
            galleryItemList.clear();
            galleryItemList.addAll(list);
            notifyDataSetChanged();
        }
    }


    private void setImage(String photoUrl, final GalleryViewHolder holder) {
        if (!"".equals(photoUrl)) {
            Picasso.with(mContext)
                    .load(photoUrl)
                    .error(R.drawable.photo_unavailable)
                    .into(holder.photoUrl);
        } else {
            holder.photoUrl.setImageResource(R.drawable.photo_unavailable);
        }
    }

    @Override
    public int getItemCount() {
        return galleryItemList.size();
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private String id;
        private String url;
        private String title;
        private ImageView photoUrl;
        private TextView tvTitle;
        private PhotoClickListener photoClickListener;

        GalleryViewHolder(View view, PhotoClickListener photoClickListener) {
            super(view);
            this.photoUrl = view.findViewById(R.id.item_gallery);
            this.tvTitle = view.findViewById(R.id.title_image);
            this.photoClickListener = photoClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            photoClickListener.onItemClick(id, url, title);
        }
    }
}
