package com.matic.flickrandroid.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.adapter.ListCommentAdapter;
import com.matic.flickrandroid.interfaces.ViewHolderClickListener;
import com.matic.flickrandroid.interfaces.VolleyCallbacks;
import com.matic.flickrandroid.manager.FlickrManager;
import com.matic.flickrandroid.model.Comment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matic on 9/11/17.
 */

public class CommentDialogFragment extends DialogFragment implements VolleyCallbacks, ViewHolderClickListener {


    private static final String TAG = CommentDialogFragment.class.getSimpleName();
    private static CommentDialogFragment instance;
    private ListCommentAdapter mAdapter;
    private RecyclerView mRecyclerView;


    public static CommentDialogFragment newInstance() {
        if (instance == null)
            instance = new CommentDialogFragment();
        return instance;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getComment();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getComment() {
        String photoId = getActivity().getIntent().getStringExtra("PHOTO_ID");
        FlickrManager fm = new FlickrManager(getContext(), this);
        fm.getCommentByPhotoId(photoId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        initCV(view);
    }

    private void initCV(View view) {
        mRecyclerView = view.findViewById(R.id.commentRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        List<Comment> commentList = new ArrayList<>();
        mAdapter = new ListCommentAdapter(commentList, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void VolleyResponse(String data) {
        populateList(data);
    }

    @Override
    public void responseUserInfo(String data) {

    }

    private void populateList(String data) {
        List<Comment> list = new ArrayList<>();
        if (data != null) {
            try {
                JSONObject jsonObject = new JSONObject(data);
                JSONArray jsonArray = jsonObject.getJSONObject("comments").getJSONArray("comment");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject comment = jsonArray.getJSONObject(i);
                    String id = comment.getString("id");
                    String username = comment.getString("authorname");
                    long datetime = comment.getLong("datecreate");
                    String realname = comment.getString("realname");
                    String content = comment.getString("_content");
                    list.add(new Comment(id, username, realname, datetime, content));
                }
            } catch (JSONException e) {
                Log.e(TAG, "VolleyResponse: ", e);
            }

            if (list.isEmpty()) {
                showNoComments();
                getDialog().dismiss();
            }
            mAdapter.addAll(list);

        } else {
            Toast.makeText(getContext(), R.string.str_no_internet, Toast.LENGTH_SHORT).show();
            getDialog().dismiss();
        }

    }

    private void showNoComments() {
        Toast.makeText(getContext(), R.string.str_comment_empty, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(String id, String url) {

    }
}
