package com.matic.flickrandroid.config;

/**
 * Created by matic on 1/9/17.
 */

public class Endpoints {

    private static final String API_KEY = "ed63c482172c41cc603ea3a1bdb01012";
    private static final String FORMAT_JSON = "&format=json&nojsoncallback=1";
    private static final String DEFAULT_USER_ID = "&user_id=150842531@N02";
    private static final String DEFAULT_PHOTO_SET_ID = "&photoset_id=72157682375406310";
    private static final String DEFAULT_USER_NAME = "&username=matiascamusso";

    public static final String URL_USERS = "https://api.flickr.com/services/rest/?method=flickr.people.findByUsername" + FORMAT_JSON + "&api_key=" + API_KEY;
    public static final String URL_PHOTO_SETS = "https://api.flickr.com/services/rest/?method=flickr.photosets.getList" + FORMAT_JSON + "&api_key=" + API_KEY;
    public static final String URL_GET_PHOTOS = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos" + "&api_key=" + API_KEY; //+ DEFAULT_PHOTO_SET_ID + DEFAULT_USER_ID;
    public static final String URL_INFO_USER = "https://api.flickr.com/services/rest/?method=flickr.people.getInfo" + FORMAT_JSON + "&api_key=" + API_KEY;

    public static final String URL_COMMENTS = "https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList" + FORMAT_JSON + "&api_key=" + API_KEY;


}
