package com.matic.flickrandroid.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.matic.flickrandroid.R;

/**
 * Created by matic on 7/9/17.
 */

public class PreferencesManager {

    public static boolean isEnglish(Context mContext) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPref.getBoolean(mContext.getString(R.string.key_lang_en), false);
    }

    public static boolean isThemePink(Context mContext) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPref.getBoolean(mContext.getString(R.string.key_theme_pink), false);
    }

    public static String getUsername(Context mContext) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPref.getString(mContext.getString(R.string.key_username), mContext.getString(R.string.str_default_username));
    }

    public static String getUserId(Context mContext) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPref.getString("key_user_id", "");
    }


}
