package com.matic.flickrandroid.support;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.matic.flickrandroid.R;

import java.util.Date;
import java.util.Locale;

/**
 * Created by matic on 7/9/17.
 */

public class Utils {

    private static int sTheme;
    public final static int THEME_BLUE = 0;
    public final static int THEME_PINK = 1;
    public final static int LANG_ES = 0;
    public final static int LANG_EN = 1;
    private static int sLang;

    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    public static void changeToTheme(Activity activity, int theme) {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    /**
     * Set the theme of the activity, according to the configuration.
     */
    public static void onActivityCreateSetTheme(Activity activity) {
        switch (sTheme) {
            case THEME_BLUE:
                activity.setTheme(R.style.ThemeBlue);
                break;
            case THEME_PINK:
                activity.setTheme(R.style.ThemePink);
                break;
        }
    }

    public static void changeToLang(Activity activity, int lang) {
        sLang = lang;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    public static void onActivityCreateSetLang(Activity activity) {
        switch (sLang) {
            case LANG_ES:
                setLocale("es", activity);
                break;
            case LANG_EN:
                setLocale("en", activity);
                break;
        }
    }

    public static void setLocale(String lang, Activity activity) {
        Locale myLocale = new Locale(lang);
        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public static long getCurrentDate(){
        Date date = new Date();
        return date.getTime();
    }


}
