package com.matic.flickrandroid.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.activity.FullscreenPhotoActivity;
import com.matic.flickrandroid.adapter.ListGalleryAdapter;
import com.matic.flickrandroid.database.DataBase;
import com.matic.flickrandroid.interfaces.PhotoClickListener;
import com.matic.flickrandroid.interfaces.VolleyCallbacks;
import com.matic.flickrandroid.manager.FlickrManager;
import com.matic.flickrandroid.model.PhotoGallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.matic.flickrandroid.support.Utils.getCurrentDate;

/**
 * Created by matic on 6/9/17.
 */

public class GalleryFragment extends Fragment implements VolleyCallbacks, PhotoClickListener, View.OnClickListener {

    private static final String TAG = GalleryFragment.class.getSimpleName();
    public static final String GALLERY_FRAGMENT = "GALLERY_FRAGMENT";
    public static final String PHOTO_ID = "PHOTO_ID";
    public static final String PHOTO_URL = "PHOTO_URL";
    public static final String PHOTO_TITLE = "PHOTO_TITLE";
    public static final String USER_ID = "USER_ID";
    public static final String ALBUM_ID = "ALBUM_ID";
    private RecyclerView mRecyclerView;
    private ListGalleryAdapter mAdapter;
    private FlickrManager flickrManager;
    private ProgressBar progressBar;
    private Button btnOrderDate;
    private Button btnOrderName;
    private DataBase dataBase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        initUI(view);
        initCardView();
        initData();
    }

    private void initUI(View view) {
        mRecyclerView = view.findViewById(R.id.galleryRecyclerView);
        progressBar = view.findViewById(R.id.progressBarGallery);
        btnOrderDate = view.findViewById(R.id.btn_order_date);
        btnOrderName = view.findViewById(R.id.btn_order_name);
        btnOrderDate.setOnClickListener(this);
        btnOrderName.setOnClickListener(this);
        btnOrderDate.setVisibility(View.INVISIBLE);
        btnOrderName.setVisibility(View.INVISIBLE);
    }

    private void initData() {
        dataBase = DataBase.getInstance(getContext());
        if (flickrManager == null) {
            flickrManager = new FlickrManager(getContext(), this);
        }

        String userId = getArguments().getString(USER_ID);
        Log.d(TAG, "userId: " + userId);
        String albumId = getArguments().getString(ALBUM_ID);
        Log.d(TAG, "albumID: " + albumId);
        if (userId != null && albumId != null) {
            flickrManager.getGalleryById(userId, albumId);
        }

        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    private void initCardView() {
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        List<PhotoGallery> galleryList = new ArrayList<>();
        mAdapter = new ListGalleryAdapter(galleryList, getContext(), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void VolleyResponse(String data) {
        Log.d(TAG, "VolleyResponse: " + data);
        populateList(data);
    }

    @Override
    public void responseUserInfo(String data) {

    }

    private void populateList(String data) {
        List<PhotoGallery> list = getPhotoGalleries(data);
        mAdapter.addAll(list);
        progressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private List<PhotoGallery> getPhotoGalleries(String data) {
        List<PhotoGallery> list = new ArrayList<>();
        if (data != null) {
            try {
                JSONObject jsonObject = new JSONObject(data);
                JSONArray jsonArray = jsonObject.getJSONObject("photoset").getJSONArray("photo");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject photo = jsonArray.getJSONObject(i);
                    String id = photo.getString("id");
                    String title = photo.getString("title");
                    int farm = photo.getInt("farm");
                    int server = photo.getInt("server");
                    String secret = photo.getString("secret");

                    String photoUrl = "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" +
                            secret + ".jpg";
                    Log.d(TAG, "image: " + photoUrl);

                    list.add(new PhotoGallery(id, photoUrl, title, getCurrentDate()));
                }
                updateButton(true);
                dataBase.saveOrUpdatePhotoGallery(list);
            } catch (JSONException e) {
                Log.e(TAG, "VolleyResponse: ", e);
            }
        } else {
            list = dataBase.getPhotosList("date");
        }
        btnOrderDate.setVisibility(View.VISIBLE);
        btnOrderName.setVisibility(View.VISIBLE);
        return list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {

        Boolean isCheckedDate;
        List<PhotoGallery> list;
        if (view.getId() == R.id.btn_order_date) {
            list = dataBase.getPhotosList("date");
            isCheckedDate = true;
        } else {
            list = dataBase.getPhotosList("title");
            isCheckedDate = false;
        }
        updateButton(isCheckedDate);
        mAdapter.clear();
        mAdapter.addAll(list);
    }

    @Override
    public void onItemClick(String id, String url, String title) {
        Log.d(TAG, "onItemClick: " + id);
        Intent intent = new Intent(this.getActivity(), FullscreenPhotoActivity.class);
        intent.putExtra(PHOTO_ID, id);
        intent.putExtra(PHOTO_URL, url);
        intent.putExtra(PHOTO_TITLE, title);
        startActivity(intent);
    }

    private void updateButton(boolean isCheckedDate) {
        if (isCheckedDate) {
            btnOrderDate.setBackground(getResources().getDrawable(R.drawable.bg_button_order));
            btnOrderDate.setTextColor(getResources().getColor(R.color.colorWhite));
            btnOrderName.setBackground(getResources().getDrawable(R.drawable.bg_button_order_outline));
            btnOrderName.setTextColor(getResources().getColor(R.color.colorAccent));
        } else {
            btnOrderDate.setBackground(getResources().getDrawable(R.drawable.bg_button_order_outline));
            btnOrderDate.setTextColor(getResources().getColor(R.color.colorAccent));
            btnOrderName.setBackground(getResources().getDrawable(R.drawable.bg_button_order));
            btnOrderName.setTextColor(getResources().getColor(R.color.colorWhite));
        }

    }

}
