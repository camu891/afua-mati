package com.matic.flickrandroid.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by matic on 2/9/17.
 */

public class Album extends RealmObject {

    @PrimaryKey
    private String id;
    private String title;
    private String description;
    private String image;
    private int photos;
    private int views;


    public Album() {
    }

    public Album(String id, String title, String description, String image, int photos, int views) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.photos = photos;
        this.views = views;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPhotos() {
        return photos;
    }

    public void setPhotos(int photos) {
        this.photos = photos;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}
