package com.matic.flickrandroid.manager;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.matic.flickrandroid.interfaces.VolleyCallbacks;

import org.json.JSONException;
import org.json.JSONObject;

import static com.matic.flickrandroid.config.Endpoints.URL_COMMENTS;
import static com.matic.flickrandroid.config.Endpoints.URL_GET_PHOTOS;
import static com.matic.flickrandroid.config.Endpoints.URL_INFO_USER;
import static com.matic.flickrandroid.config.Endpoints.URL_PHOTO_SETS;
import static com.matic.flickrandroid.config.Endpoints.URL_USERS;

/**
 * Created by matic on 1/9/17.
 */

public class FlickrManager {

    private Context mContext;
    private static final String TAG = FlickrManager.class.getSimpleName();
    private VolleyCallbacks delegate = null;

    public FlickrManager(Context mContext, VolleyCallbacks delegate) {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    public void getUserIdByUsername(String username) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = URL_USERS + "&username=" + username;
        Log.d(TAG, "getUserIdByUsername - url: " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                delegate.VolleyResponse(getUserId(response));
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                delegate.VolleyResponse(error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private String getUserId(String response) {
        String userId = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject user = jsonObject.getJSONObject("user");
            userId = user.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userId;
    }


    public void getAlbumsByUserId(String id) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = URL_PHOTO_SETS + "&user_id=" + id;
        Log.d(TAG, "getAlbumsByUserId - url: " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                delegate.VolleyResponse(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                delegate.VolleyResponse(null);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getUserInfoByUserId(String userId) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = URL_INFO_USER + "&user_id=" + userId;
        Log.d(TAG, "getUserIdByUsername - url: " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                delegate.responseUserInfo(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                delegate.responseUserInfo(null);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getGalleryById(String user_id, String album_id) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = URL_GET_PHOTOS +"&photoset_id="+ album_id + "&user_id=" + user_id + "&format=json&nojsoncallback=1";
        Log.d(TAG, "getGalleryByUserId - url: " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                delegate.VolleyResponse(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                delegate.VolleyResponse(null);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }



    public void getCommentByPhotoId(String photo_id) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = URL_COMMENTS +"&photo_id=" + photo_id;
        Log.d(TAG, "getCommentByPhotoId - url: " + url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                delegate.VolleyResponse(response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                delegate.VolleyResponse(null);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }



}
