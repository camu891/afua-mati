package com.matic.flickrandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.interfaces.ViewHolderClickListener;
import com.matic.flickrandroid.model.Album;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by matic on 2/6/17.
 */

public class ListAlbumsAdapter extends RecyclerView.Adapter<ListAlbumsAdapter.AlbumViewHolder> {

    private static final String TAG = ListAlbumsAdapter.class.getSimpleName();
    private List<Album> albumItemList;
    private Context mContext;
    private ViewHolderClickListener albumViewHolderClickListener;

    public ListAlbumsAdapter(Context context, List<Album> albumItemList, ViewHolderClickListener albumViewHolderClickListener) {
        this.albumItemList = albumItemList;
        this.mContext = context;
        this.albumViewHolderClickListener = albumViewHolderClickListener;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_album, null);
        return new AlbumViewHolder(view, albumViewHolderClickListener);
    }

    public void clear() {
        if (albumItemList != null) {
            albumItemList.clear();
            notifyDataSetChanged();
        }
    }

    public void addAll(List<Album> list) {
        Log.d(TAG, "addAll: list:" + list);
        if (albumItemList != null) {
            albumItemList.clear();
            albumItemList.addAll(list);
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder albumViewHolder, int i) {
        Album album = albumItemList.get(i);
        albumViewHolder.id = album.getId();
        albumViewHolder.tvTitleAlbum.setText(album.getTitle());
        albumViewHolder.tvDescription.setText(album.getDescription());
        albumViewHolder.photos.setText(String.valueOf(album.getPhotos()));
        albumViewHolder.views.setText(String.valueOf(album.getViews()));
        setImage(album.getImage(), albumViewHolder);
    }

    private void setImage(String imageUri, final AlbumViewHolder albumViewHolder) {
        if (!"".equals(imageUri)) {
            Picasso.with(mContext)
                    .load(imageUri)
                    .error(R.drawable.photo_unavailable)
                    .into(albumViewHolder.image);
        } else {
            albumViewHolder.image.setImageResource(R.drawable.photo_unavailable);
        }
    }

    @Override
    public int getItemCount() {
        return (null != albumItemList ? albumItemList.size() : 0);
    }

    public static class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitleAlbum;
        private String id;
        private TextView tvDescription;
        private ImageView image;
        private ProgressBar progressBar;
        private TextView photos;
        private TextView views;
        private ViewHolderClickListener albumViewHolderClickListener;

        AlbumViewHolder(View view, ViewHolderClickListener albumViewHolderClickListener) {
            super(view);
            this.tvTitleAlbum = view.findViewById(R.id.txt_title_album);
            this.tvDescription = view.findViewById(R.id.txt_desc_album);
            this.image = view.findViewById(R.id.img_flickr);
            this.progressBar = view.findViewById(R.id.loading_image);
            this.photos = view.findViewById(R.id.count_photos);
            this.views = view.findViewById(R.id.count_views);
            this.albumViewHolderClickListener = albumViewHolderClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            albumViewHolderClickListener.onItemClick(id,"");
        }
    }
}
