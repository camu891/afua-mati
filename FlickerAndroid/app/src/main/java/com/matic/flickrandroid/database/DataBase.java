package com.matic.flickrandroid.database;

import android.content.Context;
import android.util.Log;

import com.matic.flickrandroid.model.Album;
import com.matic.flickrandroid.model.PhotoGallery;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.RealmModule;

/**
 * Created by matic on 16/7/17.
 */
@RealmModule(allClasses = true)
public class DataBase {

    private static final String TAG = DataBase.class.getName();
    private static DataBase instance = null;
    private static RealmConfiguration libraryConfig;
    private Realm realm;

    public DataBase() {
    }

    public static DataBase getInstance(Context mContext) {
        if (instance == null) {
            Realm.init(mContext);
            instance = new DataBase();
            libraryConfig = new RealmConfiguration.Builder()
                    .name("flickr.realm")
                    .modules(instance)
                    .build();
        }
        return instance;
    }


    private void openDataBase() {
        try {
            realm = Realm.getInstance(libraryConfig);
        } catch (Exception e) {
            Log.e(TAG, "openDataBase - Error: " + e);
        }
    }

    private void closeDataBase() {
        if (realm != null) {
            realm.close();
        }
    }

    public void saveOrUpdateAlbum(final List<Album> albumItem) {
        try {
            openDataBase();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(Album.class);
                    realm.insertOrUpdate(albumItem);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "saveOrUpdateAlbum - onSuccess: ");
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "saveOrUpdateAlbum - Error: " + e);
            realm.cancelTransaction();
        } finally {
            closeDataBase();
        }
    }

    public void deleteAlbum() {
        RealmResults<Album> results = realm.where(Album.class).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public List<Album> getAlbumList() {
        List<Album> result = null;
        try {
            openDataBase();
            RealmQuery<Album> query = realm.where(Album.class);
            result = new ArrayList<>(query.findAll());

        } catch (Exception e) {
            Log.e(TAG, "getBarList - Error: " + e);
        }
        return result;
    }


    public void saveOrUpdatePhotoGallery(final List<PhotoGallery> photoGallery) {
        try {
            openDataBase();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(PhotoGallery.class);
                    realm.insertOrUpdate(photoGallery);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "saveOrUpdatePhotoGallery - onSuccess: ");
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "saveOrUpdatePhotoGallery - Error: " + e);
            realm.cancelTransaction();
        } finally {
            closeDataBase();
        }
    }

    public List<PhotoGallery> getPhotosList(String order) {
        List<PhotoGallery> result = null;
        try {
            openDataBase();
            RealmQuery<PhotoGallery> query = realm.where(PhotoGallery.class);
            result = new ArrayList<>(query.findAll().sort(order));
        } catch (Exception e) {
            Log.e(TAG, "getPhotosList - Error: " + e);
        }
        return result;
    }


}
