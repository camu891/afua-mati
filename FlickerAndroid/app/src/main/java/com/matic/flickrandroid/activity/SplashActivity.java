package com.matic.flickrandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.matic.flickrandroid.R;


public class SplashActivity extends AppCompatActivity {


    public static final int DELAY_MILLIS = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intentActivity = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intentActivity);
                finish();
            }
        }, DELAY_MILLIS);
    }

}
