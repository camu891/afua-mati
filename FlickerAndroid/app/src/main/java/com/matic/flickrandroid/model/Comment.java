package com.matic.flickrandroid.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by matic on 9/11/17.
 */

public class Comment extends RealmObject {

    @PrimaryKey
    private String id;
    private String username;
    private String realname;
    private long datetime;
    private String content;


    public Comment() {
    }

    public Comment(String id, String username, String realname, long datetime, String content) {
        this.id = id;
        this.username = username;
        this.realname = realname;
        this.datetime = datetime;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {

        this.datetime = datetime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
