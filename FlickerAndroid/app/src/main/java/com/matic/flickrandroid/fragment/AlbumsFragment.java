package com.matic.flickrandroid.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.adapter.ListAlbumsAdapter;
import com.matic.flickrandroid.database.DataBase;
import com.matic.flickrandroid.interfaces.ViewHolderClickListener;
import com.matic.flickrandroid.interfaces.VolleyCallbacks;
import com.matic.flickrandroid.manager.FlickrManager;
import com.matic.flickrandroid.model.Album;
import com.matic.flickrandroid.support.ViewDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.matic.flickrandroid.fragment.GalleryFragment.GALLERY_FRAGMENT;
import static com.matic.flickrandroid.preference.PreferencesManager.getUserId;

/**
 * Created by matic on 7/9/17.
 */

public class AlbumsFragment extends Fragment implements VolleyCallbacks, SwipeRefreshLayout.OnRefreshListener, ViewHolderClickListener {

    private static final String TAG = AlbumsFragment.class.getSimpleName();
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeContainer;
    private ListAlbumsAdapter mAdapter;
    private FlickrManager flickrManager;
    private ProgressBar progressBar;
    private DataBase dataBase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_albums, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        initUI(view);
        initData();
        initCardView();
        changeTitle();
    }

    private void changeTitle() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Albums");
    }

    private void initUI(View view) {
        mRecyclerView = view.findViewById(R.id.recycler_view);
        progressBar = view.findViewById(R.id.progressBarAlbums);

        swipeContainer = view.findViewById(R.id.swipe_refresh);
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.colorAccent,
                R.color.colorBlueFlickr,
                R.color.colorPrimaryDark,
                R.color.colorPinkFlickr);
    }

    private void initData() {
        dataBase = DataBase.getInstance(getContext());
        if (flickrManager == null) {
            flickrManager = new FlickrManager(getContext(), this);
        }

        String userId = getArguments().getString(USER_ID);
        if (userId != null) {
            saveUserId(userId);
            flickrManager.getUserInfoByUserId(userId);
            flickrManager.getAlbumsByUserId(userId);
        }

        progressBar.setVisibility(View.VISIBLE);
        swipeContainer.setVisibility(View.GONE);
    }

    private void saveUserId(String userId) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.key_user_id), userId);
        editor.apply();
    }

    private void initCardView() {
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        List<Album> albumList = new ArrayList<>();
        mAdapter = new ListAlbumsAdapter(getContext(), albumList, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(String id, String url) {
        goToInfoGallery(id);
    }

    private void goToInfoGallery(String id) {
        Log.d(TAG, "goToInfoGallery: ");
        FragmentManager fragmentManager = getFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("ALBUM_ID", id);
        bundle.putString("USER_ID", getUserId(getContext()));
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.main_content, galleryFragment, GALLERY_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onDestroyView() {
        destroyListeners();
        super.onDestroyView();
    }

    private void destroyListeners() {
        swipeContainer.setOnRefreshListener(null);
    }


    @Override
    public void VolleyResponse(String data) {
        Log.d(TAG, "VolleyResponse: " + data);
        populateList(data);
        if (swipeContainer != null)
            swipeContainer.setRefreshing(false);
    }

    @Override
    public void responseUserInfo(String data) {
        //nothing
    }

    private void populateList(String data) {
        List<Album> list = new ArrayList<>();

        if (data != null) {
            try {
                JSONObject jsonObject = new JSONObject(data);
                JSONArray jsonArray = jsonObject.getJSONObject("photosets").getJSONArray("photoset");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject photoset = jsonArray.getJSONObject(i);
                    Log.d(TAG, "Photoset Array: " + photoset);
                    String id = photoset.getString("id");
                    Log.d(TAG, "Photoset ID: " + id);
                    int photos = photoset.getInt("photos");
                    int views = photoset.getInt("count_views");
                    JSONObject joTitle = photoset.getJSONObject("title");
                    String title = joTitle.getString("_content");
                    JSONObject joDesc = photoset.getJSONObject("description");
                    String description = joDesc.getString("_content");

                    int farm = photoset.getInt("farm");
                    int server = photoset.getInt("server");
                    long primary = photoset.getLong("primary");
                    String secret = photoset.getString("secret");

                    String image = "https://farm" + farm + ".staticflickr.com/" + server + "/" + primary + "_" + secret + ".jpg";
                    Log.d(TAG, "image: " + image);

                    list.add(new Album(id, title, description, image, photos, views));
                }

                dataBase.saveOrUpdateAlbum(list);
                Toast.makeText(getContext(), "Albums actualizados!", Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                Log.e(TAG, "VolleyResponse: ", e);
            }

        } else {
            //busca de la bd
            ViewDialog viewDialog = new ViewDialog();
            list = dataBase.getAlbumList();

            String message;
            if (list.isEmpty()) {
                message = "No hay conexión.\n No se han encontrado albums cargados.";
            } else {
                message = "No hay conexión. \nSe mostrarán los últimos albums encontrados.";
            }
            viewDialog.showDialog(getActivity(), message);

        }

        mAdapter.addAll(list);
        swipeContainer.setOnRefreshListener(this);
        progressBar.setVisibility(View.GONE);
        swipeContainer.setVisibility(View.VISIBLE);
    }


    @Override
    public void onRefresh() {
        initData();
    }


}
