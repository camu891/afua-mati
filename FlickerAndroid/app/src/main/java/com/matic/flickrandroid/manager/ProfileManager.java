package com.matic.flickrandroid.manager;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.matic.flickrandroid.R;
import com.matic.flickrandroid.support.CircleTransform;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by matic on 7/9/17.
 */

public class ProfileManager {

    private static final String TAG = ProfileManager.class.getSimpleName();
    private Context mContext;
    private ImageView logoProfile;
    private TextView tvUsername;
    private TextView tvRealname;

    public ProfileManager(Context mContext, ImageView logoProfile, TextView tvUsername, TextView tvRealname) {
        this.mContext = mContext;
        this.logoProfile = logoProfile;
        this.tvUsername = tvUsername;
        this.tvRealname = tvRealname;
    }

    public void setProfile(String data) {

        if (data != null) {
            try {
                Log.d(TAG, "setProfile: " + data);
                JSONObject jsonObject = new JSONObject(data);
                JSONObject person = jsonObject.getJSONObject("person");
                JSONObject joUsername = person.getJSONObject("username");
                String username = joUsername.getString("_content");
                JSONObject joRealname = person.getJSONObject("realname");
                String realname = joRealname.getString("_content");

                String nsid = person.getString("nsid");
                int iconFarm = person.getInt("iconfarm");
                int iconServer = person.getInt("iconserver");

                String url = "http://farm" + iconFarm + ".staticflickr.com/" + iconServer + "/buddyicons/" + nsid + "_r.jpg";

                Log.d(TAG, "setProfile: " + url);
                if (username != null && realname != null) {
                    tvRealname.setText(realname);
                    tvUsername.setText(username);
                }

                Glide.with(mContext)
                        .load(url)
                        .transform(new CircleTransform(mContext))
                        .error(R.mipmap.ic_launcher)
                        .into(logoProfile);

                //send broadcast para que cambie el username en home

            } catch (JSONException e) {
                Log.e(TAG, "toolbarProfile - JSONException: ", e);
                Toast.makeText(mContext, mContext.getString(R.string.str_error_username), Toast.LENGTH_SHORT).show();
            }

        } else {
            //no hay conexion
        }


    }

}
