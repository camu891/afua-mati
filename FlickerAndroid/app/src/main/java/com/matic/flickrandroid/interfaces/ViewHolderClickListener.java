package com.matic.flickrandroid.interfaces;

/**
 * Created by matic on 2/9/17.
 */

public interface ViewHolderClickListener {
    void onItemClick(String id, String url);

    //void onItemClick(String id);
}

