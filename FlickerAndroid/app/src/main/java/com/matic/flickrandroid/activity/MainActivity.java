package com.matic.flickrandroid.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.fragment.HomeFragment;
import com.matic.flickrandroid.interfaces.VolleyCallbacks;
import com.matic.flickrandroid.manager.FlickrManager;
import com.matic.flickrandroid.manager.ProfileManager;
import com.matic.flickrandroid.preference.PreferencesManager;
import com.matic.flickrandroid.support.Utils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, VolleyCallbacks, View.OnClickListener {


    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String HOME_FRAGMENT = "home_fragment";
    private DrawerLayout drawerLayout;
    private SwitchCompat drawerSwitchLang;
    private SwitchCompat drawerSwitchTheme;
    private ProfileManager profileManager;
    private FlickrManager flickrManager;
    private DialogInterface.OnClickListener onclickPositiveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetLang(this);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_main);
        initToolbar();
        initHome(savedInstanceState);
        getUserInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if(PreferencesManager.isEnglish(this)){
//            Utils.changeToLang(this, Utils.LANG_EN); //en
//        }
//        if(PreferencesManager.isThemePink(this)){
//            Utils.changeToTheme(this, Utils.THEME_PINK);
//        }
    }

    public void getUserInfo() {
        if (flickrManager == null) {
            flickrManager = new FlickrManager(getApplicationContext(), this);
        }
        flickrManager.getUserIdByUsername(PreferencesManager.getUsername(getApplicationContext()));
    }

    private void initHome(Bundle savedInstanceState) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment homeFragment;
        if (savedInstanceState == null) {
            homeFragment = new HomeFragment();
        } else {
            homeFragment = fragmentManager.findFragmentByTag(HOME_FRAGMENT);
        }
        fragmentManager.beginTransaction().replace(R.id.main_content, homeFragment, HOME_FRAGMENT).commit();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(null);
        getSupportActionBar().setTitle(null);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_nav);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView mNavigationView = (NavigationView) findViewById(R.id.navview);
        mNavigationView.setNavigationItemSelectedListener(this);

        View headerView = LayoutInflater.from(this).inflate(R.layout.layout_header, mNavigationView, false);
        mNavigationView.addHeaderView(headerView);
        initUser(headerView);
        initSwitchDrawer(mNavigationView);
    }

    private void initSwitchDrawer(NavigationView navView) {
        Menu menuTheme = navView.getMenu();
        drawerSwitchLang = menuTheme.findItem(R.id.menu_language).getActionView().findViewById(R.id.drawer_switch_lang);
        drawerSwitchTheme = menuTheme.findItem(R.id.menu_theme).getActionView().findViewById(R.id.drawer_switch_theme);
        drawerSwitchLang.setOnClickListener(this);
        drawerSwitchTheme.setOnClickListener(this);

        boolean isEnglish = PreferencesManager.isEnglish(this);
        drawerSwitchLang.setChecked(isEnglish);
        boolean isPink = PreferencesManager.isThemePink(this);
        drawerSwitchTheme.setChecked(isPink);
    }

    private void initUser(View headerView) {
        TextView username = headerView.findViewById(R.id.header_username);
        TextView realname = headerView.findViewById(R.id.header_realname);
        ImageView logoUser = headerView.findViewById(R.id.header_img);
        profileManager = new ProfileManager(getApplicationContext(), logoUser, username, realname);
        headerView.setOnClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_home:
                clearStack();
                drawerLayout.closeDrawers();
                break;
            case R.id.menu_language:
                setLanguage();
                break;
            case R.id.menu_theme:
                setTheme();
                break;
            default:
                break;
        }
        return true;
    }

    private void setTheme() {
        if (drawerSwitchTheme.isChecked()) {
            drawerSwitchTheme.setChecked(false);
            savePreferenceTheme(false);
            Utils.changeToTheme(this, Utils.THEME_BLUE);
        } else {
            drawerSwitchTheme.setChecked(true);
            savePreferenceTheme(true);
            Utils.changeToTheme(this, Utils.THEME_PINK);
        }
    }

    private void setLanguage() {
        if (drawerSwitchLang.isChecked()) {
            drawerSwitchLang.setChecked(false);
            savePreferenceLang(false);
            Utils.changeToLang(this, Utils.LANG_ES); //es
        } else {
            drawerSwitchLang.setChecked(true);
            savePreferenceLang(true);
            Utils.changeToLang(this, Utils.LANG_EN); //en
        }
    }

    private void clearStack() {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        while (count > 0) {
            fm.popBackStack();
            count--;
        }
    }

    private void savePreferenceLang(boolean isEnglish) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.key_lang_en), isEnglish);
        editor.apply();
    }

    private void savePreferenceTheme(boolean isPink) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.key_theme_pink), isPink);
        editor.apply();
    }

    @Override
    public void VolleyResponse(String data) {
        Log.d(TAG, "User ID: " + data);
        if (data != null) {
            flickrManager.getUserInfoByUserId(data);
        }
    }

    @Override
    public void responseUserInfo(String data) {
        Log.d(TAG, "responseUserInfo: " + data);
        profileManager.setProfile(data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header:
                openDialogUsername();
                break;
            case R.id.drawer_switch_lang:
                setLanguage();
                break;
            case R.id.drawer_switch_theme:
                setTheme();
                break;

        }

    }

    private void openDialogUsername() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_username_default_title));
        View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.username_input,
                (ViewGroup) findViewById(android.R.id.content), false);
        EditText inputUsername = viewInflated.findViewById(R.id.et_username);
        builder.setView(viewInflated);
        onclickPositiveButton = new OnclickPostitiveButtonDistance(getApplicationContext(), inputUsername, this);
        builder.setPositiveButton(getString(R.string.str_ok), onclickPositiveButton);
        builder.setNegativeButton(getString(R.string.str_cancel), null);

        builder.show();
    }

    private static class OnclickPostitiveButtonDistance implements DialogInterface.OnClickListener {

        private Context mContext;
        private EditText inputUsername;
        private MainActivity mainActivity;

        public OnclickPostitiveButtonDistance(Context mContext, EditText inputUsername, MainActivity mainActivity) {
            this.mContext = mContext;
            this.inputUsername = inputUsername;
            this.mainActivity = mainActivity;
        }

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            saveUsername(inputUsername.getText().toString());
        }

        private void saveUsername(String username) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(mContext.getString(R.string.key_username), username);
            editor.apply();
            mainActivity.getUserInfo();
        }

    }




}
