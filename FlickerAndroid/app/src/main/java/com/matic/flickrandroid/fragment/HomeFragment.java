package com.matic.flickrandroid.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.interfaces.VolleyCallbacks;
import com.matic.flickrandroid.manager.FlickrManager;
import com.matic.flickrandroid.preference.PreferencesManager;

/**
 * Created by matic on 7/9/17.
 */

public class HomeFragment extends Fragment implements View.OnClickListener, VolleyCallbacks {


    private static final String TAG = HomeFragment.class.getSimpleName();
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String ALBUM_FRAGMENT = "album_fragment";
    private EditText usernameEditText;
    private ProgressDialog dialog;
    private FlickrManager flickrManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        initUI(view);
    }

    private void initUI(View view) {
        String username = PreferencesManager.getUsername(getContext());
        usernameEditText = view.findViewById(R.id.username_et);
        usernameEditText.setText(username);
        Button findButton = view.findViewById(R.id.find_button);
        findButton.setOnClickListener(this);//le paso el this que es el contexto porq esta implementado ahi arriba
        //no hicimos el new on click listener adentro porq genera memory leaks y es mejor definirlo en el onclick. y
        //sino haces una clase interna que implemente onclicklistener
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.find_button) {
            getUserID();
        }
    }

    private void getUserID() {
        dialog = ProgressDialog.show(getContext(), "", getString(R.string.str_loading_user), true);
        if (flickrManager == null) {
            flickrManager = new FlickrManager(getContext(), this);
        }
        flickrManager.getUserIdByUsername(usernameEditText.getText().toString());
    }


    @Override
    public void VolleyResponse(String data) {
        Log.d(TAG, "VolleyResponse: " + data);
        if (data != null && !"".equals(data)) {
            FragmentManager fragmentManager = getFragmentManager();
            Bundle bundle = new Bundle();
            bundle.putString(USER_ID, data);
            AlbumsFragment albumsFragment = new AlbumsFragment();
            albumsFragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.main_content, albumsFragment, ALBUM_FRAGMENT)
                    .addToBackStack(null)
                    .commit();

        } else {
            Toast.makeText(getContext(), getString(R.string.str_error_username), Toast.LENGTH_SHORT).show();
        }
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


    @Override
    public void responseUserInfo(String data) {
//nothing
    }


}
