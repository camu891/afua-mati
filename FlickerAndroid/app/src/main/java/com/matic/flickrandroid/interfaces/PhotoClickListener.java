package com.matic.flickrandroid.interfaces;

/**
 * Created by matic on 2/9/17.
 */

public interface PhotoClickListener {
    void onItemClick(String id, String url, String title);

    //void onItemClick(String id);
}

