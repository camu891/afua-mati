package com.matic.flickrandroid.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Afua on 15/9/2017.
 */

public class PhotoGallery extends RealmObject {

    @PrimaryKey
    private String id;
    private String photoUrl;
    private String title;
    private long date;

    public PhotoGallery() {
    }

    public PhotoGallery(String id, String photoUrl, String title, long date) {
        this.id = id;
        this.photoUrl = photoUrl;
        this.title = title;
        this.date = date;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
