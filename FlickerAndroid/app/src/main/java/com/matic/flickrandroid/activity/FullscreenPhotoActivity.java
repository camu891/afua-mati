package com.matic.flickrandroid.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matic.flickrandroid.R;
import com.matic.flickrandroid.fragment.CommentDialogFragment;


public class FullscreenPhotoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = FullscreenPhotoActivity.class.getSimpleName();
    public static final String PHOTO_ID = "PHOTO_ID";
    public static final String PHOTO_URL = "PHOTO_URL";
    public static final String PHOTO_TITLE = "PHOTO_TITLE";
    private Toolbar toolbarTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_photo);
        initToolbar();
        initUI();
    }

    private void initToolbar() {
        toolbarTop = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbarTop);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void initUI() {
        ImageView ivPhoto = (ImageView) findViewById(R.id.photo);
        Button btnComment = (Button) findViewById(R.id.btn_comment);
        Button btnShareEmail = (Button) findViewById(R.id.btn_share_email);
        Button btnOpenBrowser = (Button) findViewById(R.id.btn_open_browser);

        String photoUrl = getIntent().getStringExtra(PHOTO_URL);
        String photoTitle = getIntent().getStringExtra(PHOTO_TITLE);

        TextView mTitle = toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText(photoTitle.toUpperCase());

        btnComment.setOnClickListener(this);
        btnOpenBrowser.setOnClickListener(this);
        btnShareEmail.setOnClickListener(this);

        Glide.with(this).load(photoUrl).placeholder(R.drawable.thumbnail).into(ivPhoto);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_comment:
                openCommentDialog();
                break;
            case R.id.btn_open_browser:
                openBrowser();
                break;
            case R.id.btn_share_email:
                sendEmail();
                break;
        }
    }


    private void openCommentDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        DialogFragment commentFragment = CommentDialogFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(PHOTO_ID, getIntent().getStringExtra(PHOTO_ID));
        commentFragment.setArguments(bundle);
        commentFragment.show(ft, "commentFragment");
    }

    private void openBrowser() {
        String photoUrl = getIntent().getStringExtra(PHOTO_URL);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(photoUrl));
        startActivity(intent);
    }

    private void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.str_photo_from) +" "+ getString(R.string.app_name));
        String photoUrl = getIntent().getStringExtra(PHOTO_URL);
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, photoUrl);
        startActivity(Intent.createChooser(emailIntent, getString(R.string.str_share_choose)));
    }

}
