package com.matic.flickrandroid.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.matic.flickrandroid.R;
import com.matic.flickrandroid.interfaces.ViewHolderClickListener;
import com.matic.flickrandroid.model.Comment;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Afua on 15/9/2017.
 */

public class ListCommentAdapter extends RecyclerView.Adapter<ListCommentAdapter.CommentViewHolder> {
    private static final String TAG = ListCommentAdapter.class.getSimpleName();
    private List<Comment> commentItemList;
    private ViewHolderClickListener commentViewHolderClickListener;

    public ListCommentAdapter(List<Comment> commentItemList, ViewHolderClickListener commentViewHolderClickListener) {
        this.commentItemList = commentItemList;
        this.commentViewHolderClickListener = commentViewHolderClickListener;
    }


    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_item, null);
        return new CommentViewHolder(view, commentViewHolderClickListener);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Comment comment = commentItemList.get(position);
        holder.id = comment.getId();
        holder.realname.setText(!comment.getRealname().isEmpty() ? comment.getRealname() : comment.getUsername());
        holder.date.setText(getDate(comment.getDatetime()));
        holder.content.setText(comment.getContent());
    }

    private String getDate(long datetime) {
        Timestamp stamp = new Timestamp(datetime);
        Date date = new Date(stamp.getTime() * 1000);
        return date.toString();
    }

    public void clear() {
        if (commentItemList != null) {
            commentItemList.clear();
            notifyDataSetChanged();
        }
    }

    public void addAll(List<Comment> list) {
        Log.d(TAG, "addAll: list:" + list);
        if (commentItemList != null) {
            commentItemList.clear();
            commentItemList.addAll(list);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return commentItemList.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private String id;
        private TextView realname;
        private TextView content;
        private TextView date;
        private ViewHolderClickListener commentViewHolderClickListener;

        CommentViewHolder(View view, ViewHolderClickListener commentViewHolderClickListener) {
            super(view);
            this.realname = view.findViewById(R.id.tv_realname);
            this.date = view.findViewById(R.id.tv_date);
            this.content = view.findViewById(R.id.tv_content);
            this.commentViewHolderClickListener = commentViewHolderClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            commentViewHolderClickListener.onItemClick(id, "");
        }
    }
}
